 - Paradigma für die Strukturierung und Nutzung verteilter Funktionalität, die von unterschiedlichen Besitzern verantwortet wird
 - Architekturkonzept - losgelöst von jeglicher Technologie
 - Methode, vorhandene Komponentenvon Systemen so zu koordinieren (Orchestrierung), dass ihre Leistungen zu Diensten (Prozessen) zusammengefasst und anderen zur Verfügung gestellt werden

### Funktionalität
- Aufgabe
- Prozesse
- Layer im Programm/System
- Effekt

### OASIS
- Organization for the Advancement of Structured Information Standards

### Vorgehensweise in der Umsetzung

| Umsetzung          | Prinzip              | Wo                                                   |
| ------------------ | -------------------- | ---------------------------------------------------- |
| Neukonzeption      | Bottom-Up            | keine IT vorhanden / Start-Up                        |
| Redesign           | Top-Down             | bei vorhandenen / nicht optimalen Geschäftsprozessen |
| Meet-In-The-Middle | Top-Down + Bottom-Up | Umstrukturierung im laufenden Betrieb                                                     |
