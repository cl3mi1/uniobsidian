- Verteiltheit
- Verzeichnisdienst (Service Lookup)
- Lose Kopplung (bei Bedarf dynamische Suche nach Diensten (zur Laufzeit) und Anbindung (google suche in eigene Website)

#### Grundsätze 2
- Standards (gewährleisten eine Kommunikation d. Dienste verschiedener Dienstanbieter untereinander
#### Grundsätze 3
- Prozessorientiert (Kommunikation zw. mehreren Diensten läuft meist von alleine (autark) ab, durch Interaktion Mitarbeiter wird Prozessablauf gesteuert, Grobe Abläufe können im Voraus modelliert werden
#### Grundsätze 4
- Sicherheit (http, https, Security Tokens, IP-Whitelisting)