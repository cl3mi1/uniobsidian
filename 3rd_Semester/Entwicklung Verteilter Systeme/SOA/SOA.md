### Eigenschaften verteilter Systeme
- kein gemeinsamer Speicher (Interaktion durch Nachrichtenaustausch)
- nebenläufige/parallele Aktivitäten (Koordination, Synchronisation)
- Fehler und Ausfälle wahrscheinlich
- Komponenten (Hard- & Software) sind heterogen
- Systeme können sehr groß sein (Großsystemeffekt → Umschlag v. Quanti auf Quali)

### Wozu?
- Geschäftsprozesse (Funktionen) werden in jew. Applikationen der Fachbereiche umgesetzt
- Ein Prozess/Service aber untersch. Implementierung und ev. untersch. Zugriff auf Datenspeicher
- Application Dependent Business Function

### Das Ziel -> statt closed – monolithic – brittle
- Shared services - Collaborative - Interoperable - Integrated