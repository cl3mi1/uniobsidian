#### Schnittstellenorientiert:
    - typgesicherte Parameter-Objekte
    - synchron
#### Nachrichtenorientiert
    - Schwerpunkt liegt auf auszutauschenden Nachrichten/Docs
    - bevorzugt asynchron
##### Ressourcenorientiert
    - REST (Representational State Transfer)
    - GET, POST, PUT, DELETE sollen/müssen idempotent sein (bei mehrfacher Anwendung selbes Ergebnis)
#### funktionale Metainfos definieren
    - adresse, Schnittstellenbeschreibung
    - Funktionalität
##### nichtfunktionale Metainformationen
    - Sicherheitsanforderungen, Berechtigungen
    - Transaktionseigenschaften
    - Nachrichtenübertragungsverfahren
    - QoS, Performance, Verfügbarkeit, Auditing
    - Abrechnung