### Multi Tier Architektur
- Webbrowser - Webserver (PHP) - Datenbank (MySQL, PostgreSQL)

### Unterschiedliche Besitzer
- Jeweil. Fachabteilung (QM, Marketing etc.)
- Führungsebene
- Externe Dienstleister
- Softwareanbieter/Lizenzen

### Stacey Matrix
- Um Komplexität eines Projekts zu analysieren und einzuordnen
- Komp. korreliert sehr häufig mit der daraus resultierenden IT-Umgebung

### Service Lookup
- Veröffentlichung → Dienstverzeichnis → Dienstkonsument (Suche) → Dienstanbieter (veröffentlicht & Bindung+Nutzung zu konsum)