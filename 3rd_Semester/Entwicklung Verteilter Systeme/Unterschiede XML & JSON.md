## Json
- austauschformat für strukturierte Daten
- keine Semantik
- keine namespaces
- lightweight
- spart ca 30% der Übertragungsgröße
- muss vorgegebener Struktur folgen
- JSON.parse parser schneller und einfacher
- seit 2022: JSON Schema für Datenprüfung und Metainformationen
	- beschreibt vorliegendes Datenformat
	- validiert Daten

## XML
- Austauschformat für semistrukturierte Daten
- beschäftigt sich mit Semantik
- namespaces
- Trennt Form und Inhalt
- mehr RAM benötigt für lesen der Daten