- Regeln/Deklarationen zur Prüfung von Struktur/Schachtelung und Reihung von bestimmten Elementen
- intern oder extern eingebunden
- intern
	- `<!DOCTYPE Wurzelelement [ <!ELEMENT elementname (Regeln)>`
- extern
	- `<!DOCTYPE Literatur SYSTEM “vorlesung.dtd”>` (lokal)
	- `<!DOCTYPE Literatur SYSTEM “[http://blabla.at/vorlesung.dtd](http://blabla.at/vorlesung.dtd)>` (global)

### Elemente
`<!ELEMENT Name Inhaltstyp>`
`EMPTY` - Inhalt ist leer
`ANY` - Inhalt ist unbestimmt/beliebig
`(Regel)` - Inhalt besteht aus angegebenen Elementen
`#PCDATA` - Inhalt besteht aus Text (Parsed Character Data)
				0 bis beliebig Zeichen lang
				`#PCDATA` kann nur als Bestandteil einer Regel auftreten
				Es gelten noch weitere einschränkende Regeln

`<!ELEMENT vorlesungen (vorlesung+)>`
vorlesungen - Elementname
vorlesung - Grammatikregel

Operator          Erläuterung
`,`                        Lineare Aneinanderreihung
`+`                        1-fache bis beliebige Wiederholung
`*`                        0-fache bis beliebige Wiederholung
`?`                        Optionaler Teil
`|`                        Alternative
`()`                      Klammern des Wirkungsvereiches

### Attlist
`<!ATTLIST ElementName Attributname Inhaltstyp Ergänzung>`

CDATA - string
ID - Eindeutige identifikation(Wert darf nicht mit Ziffern beginnen)
(A|B|...) - Aufzählungstyp
NMTOKEN - XML-Name als Attributwert
NMTOKENS - Liste von XML-Namen als Attributwert
IDREF - ID als Attributwert
IDREFS - Liste von IDs als Attributwert
ENTITY - Objekt liegt außerhalb
ENTITIES - Liste von außerhalb liegenden Objekten

### Types
`<!ATTLIST vorlesung in (CDATA) #IMPLIED>`
`#REQUIRED` - gefordert
`#IMPLIED` - optional
`#FIXED XXX` - muss immer Wert XXX haben
`"XXX"` Fehlt das Attribut, ist es mit Wert XXX definiert

### Stylesheets
`<?xml-stylesheet type="text/css" href="style.css"?>`
