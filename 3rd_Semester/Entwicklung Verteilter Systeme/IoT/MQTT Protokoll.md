- seit 1999
- Machine to Machine Nachrichtenprotokoll
- publish/subscribe Architektur
- bandbreiteneffizient und energiesparend
- Quality of Service

### Ansatz
- Klassisch
	- Sensor leitet Daten direkt zu Gerät weiter
- MQTT Ansatz
	- Broker dazwischen, erhält Daten
	- Endgeräte subscriben, um Nachrichten von Broker zu erhalten, der die Daten pusht
