### Vorteile

- Sehr gute Skalierbarkeit für Publishing von “unkritischen” Daten
- Reduzierung auf das Notwendigste (Bandbreite und Energie)
- MQTT seit 2014 OASIS Standard
- Sehr guter Open Source Stack vorhanden
- Unterstützung in allen gängigen Cloud-Services

### Nachteile

- Konfiguration der Sensoren bzw. Steuerung sehr früh in der technologischen Entwicklung
- Kommerzielle Anbieter sehr teuer für den Einstieg
- Anbindung von persistierenden Schichten als Flaschenhals