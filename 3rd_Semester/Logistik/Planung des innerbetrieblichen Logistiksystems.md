Bei der Planung muss berücksichtigt werden:
- Planungsergebnisse der Fabrikplanung
- Fertigungssystemplanung
- Planung des Montagesystems
daraus ergeben sich die Anforderungen und Randbedingungen an das Logistiksystem
wie zb:
- die erforderlichen Transportleistung
- dem zur Verfügung stehenden Platzangebot
- möglichen und sinnvollen Anordnungsbeziehungen

Bevor verschiedene Ausführungsformen von Lager-, Transport- und Handhabungssystemen vorgestellt werden, muss die Aufgabe und Bedeutung des innerbetrieblichen Logistiksystems definiert werden. Anschließend werden der Planungsablauf und Hilfsmittel zur Planung vorgestellt. 

Bei der Planung des Logistiksystems entstehen die Randbedingungen und Einflußgrößen für die Planung von Fertigungs- und Montagesystem.
Bei der Planung einer Fabrik die Planung des Fertigungs-, Montage- und Logistiksystems hat die Abstimmung gleichzeitig und enger zueinander zu erfolgen.