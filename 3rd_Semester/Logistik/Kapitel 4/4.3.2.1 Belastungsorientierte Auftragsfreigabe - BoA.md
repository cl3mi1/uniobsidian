Belastungsorientierte Auftragsfreigabe (BoA) im Gegensatz zum genauen MRP II.

BoA basiert auf statistischer Betrachtung und dem „Trichtermodell“, nutzt Auftragsvorrat als Steuergröße.

Grundgedanke: Darstellung des Zusammenhangs zwischen Durchlaufzeit, Auftragsbestand, Kapazität und Leistung für ein Arbeitssystem.

Berücksichtigt Aufträge innerhalb festgelegter Zeitrahmen (Terminschranke), ordnet Aufträge nach Dringlichkeit und plant basierend auf verfügbaren Kapazitäten.

Einsatzvoraussetzungen: Detaillierte Fertigungsanalyse, konstantes Produktspektrum, feste Liefertermine, bekannte Kapazitäten, gesicherte Materialverfügbarkeit und Nutzung einer Betriebsdatenerfassung.