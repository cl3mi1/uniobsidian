Arbeitssteuerung ist Teil der Arbeitsvorbereitung und umfasst die Planung, Steuerung und Kontrolle des Teilefertigungs- und Montageprozesses.

Die Hauptaufgaben umfassen die Festlegung von Zeitpunkt, Ort, Häufigkeit und Verantwortlichkeiten für die Herstellung von Teilen oder Erzeugnissen in der Fertigung.

Arbeitssteuerung nutzt Vorgaben aus der Materialdisposition und Arbeitsplanung, um Fertigungsaufträge zu erstellen, die Arbeitsanweisungen und Terminvorgaben enthalten.

Ziel ist es, Fertigungsaufträge termingerecht und kostengünstig umzusetzen, indem Planung, Steuerung und Kontrolle des Produktionsprozesses durchgeführt werden.

Die Arbeitssteuerung verwendet verschiedene Planungs- und Steuerungsmethoden sowie Rechnerhilfsmittel und Qualitätsmanagementmethoden.

Informationen aus der Arbeitssteuerung werden an direkte und indirekte Bereiche weitergegeben, und zukünftige Entwicklungen der Arbeitssteuerung werden skizziert.