Arbeitssteuerung mit zwei künftigen Zielrichtungen:
  - Dezentralisierung von Planungsaufgaben: Verschiebung hin zur groben Vorausplanung, Mitarbeiter vor Ort übernehmen Planung, Steuerung und Kontrolle der Arbeitsschritte.
  - Erhöhter Einsatz von Informationssystemen: Verbesserte Mitarbeiterinformation über Fertigungsstand, schnellere Datenqualität und Neuplanungsgeschwindigkeit.
  
- Zukunftsaussicht: MRP II-Verfahren werden voraussichtlich mit realen Produktionsdaten in frühen Planungsstufen arbeiten können, obwohl sie heute noch nicht in der Lage dazu sind aufgrund fehlender ganzheitlicher Steuerungsfähigkeit.