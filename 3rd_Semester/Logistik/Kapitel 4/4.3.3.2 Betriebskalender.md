Ein Betriebskalender legt die Arbeitstage eines Unternehmens fest und erleichtert rechnerische Operationen, die tages- oder schichtbezogen sind.

Ein Normalkalender numeriert Kalendertage ab einem bestimmten Stichtag. Zwei Varianten sind üblich:
  - Nur die tatsächlichen Arbeitstage werden nummeriert.
  - Alle Kalendertage werden nummeriert, wobei Arbeitstage und freie Tage gekennzeichnet werden.

Die zweite Variante ermöglicht eine einfachere Unterstützung durch EDV-Systeme im Unternehmen, z.B. bei der Definition von Überstunden an arbeitsfreien Tagen.