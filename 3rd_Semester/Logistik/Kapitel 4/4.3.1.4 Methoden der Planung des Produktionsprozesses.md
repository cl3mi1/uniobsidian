Verfahren der Produktionsprozessplanung können nach zwei Kriterien klassifiziert werden:
  - Global vs. Lokal: Globale Verfahren betrachten den gesamten zu planenden Bereich, während lokale Verfahren Untereinheiten in einem Bereich betrachten.
  - Deterministisch vs. Heuristisch: Deterministische Verfahren arbeiten analytisch oder statistisch genau, während heuristische Verfahren auf Erfahrungswerten basieren.
  
- Globale Verfahren berücksichtigen die Auftragsreihenfolgeplanung über alle Aufträge und Ressourcen eines Bereichs.
- Lokale Verfahren optimieren die Planung auf der Ebene von Untereinheiten mit lokalen Optimierungskriterien.