# Logisitk Zusammenfassung

## Aufgabe des Logistiksystems

- Materialfluss zwischen einzelnen Betriebsbereichen sowie einzelnen Betriebsmitteln und maschinennahen Umfeld sicherstellen
- hinsichtlich der Auftragsabfolge, des Auftragstyps und des Produktspektrums möglichst flexibel zu bleiben.

### Struktuierung des innerbetrieblichen Logsistiksystems
Planung Logistiksystem nicht isoliert von der Planung des zugehörigen Informationssystems
Logistiksysteme sind auf zeitgerechte und zuverlässige Vorgaben des Informationssystems angewiesen

- bei manueller Disposition und Steuerung des Materialflusses treten häufig lange Liege- bzw. Durchlaufzeiten auf
- zumindest teilweise Automatisierung des Informationsflusses ist eine Vorraussetzung für automatisierte Transportvorgänge

### Bedeutung des Logistiksystems

Wertsteigerung eines Produkts wird durch Bearbeitung in der Auftragshauptzeit erreicht
außerhalb der reinen Bearbeitungszeit bringen z.B. Lager- und Transportvorgänge starken Wertzuwachs (während Ausführung ist sehr viel Kapital gebunden)

Funktionierender Materialfluss messbar an Transport-, Liege- und Verweilzeit

Reduzierung der Liegezeit bietet großes Potential für Optimierung:
    - geringere Durchlaufzeit
    - geringere Kosten des Endprodukts

### Anforderungen an die Planung des Logistiksystems
Ziel: minimale Durchlaufzeit bei variablen Losgrößen udn möglicherweise variablen Produktionsstrukturen zu erreichen
- hohe flexibilität nur erreichbar mit möglichst wenig Rückwirkung auf produzierendes System
Vermieden werden sollen Puffer, die Materialfluss von Bearbeitungsreihenfolge und Bearbeitungsstationen entklppeln und/oder Liegezeiten erfordern

variable Losgrößen beeinflussen:
    - Dimensionierung der Komponenten des Systems
    - Entscheidung, ob es sich lohnt, einen Vorgang zu automatisieren

## Systeme und Komponenten
### Lagersysteme

Lagern ist jedes geplante Liegen des Arbeitsgegenstandes im Materialfluss

Ein Lager ist ein Raum/Fläche zum Aufbewahren von Material, das mengen- und/oder wertmäßig erfasst wird.

#### Aufgabe
    - Bevorraten
    - Puffern
    - Verteilen von Gütern

#### Verschiedene Arten
Vorratslager:
    - sichert Lieferbereitschaft
    - gleicht lang- und mittelfristige Bedarfsschwankungen aus
Pufferlager:
    - gleicht kurzfristige Bedarfsschwankungen aus
Verteillager:
    - Bevorratungsfunktion
    - Kommissionierfunktion
Speicher:
    - technische Einrichtungen, die Materialien transportieren und zeitliche Ausgleichfunktion zwischen Bearbeitungsstationen ausführen

#### Organisation von Lagersystemen
- Dynamische Betrachtung mit kurzer Verweildauer der Güter
- notwendiger Bestandteil des Logistiksystems
- zentral oder dezentral
- Lagerorganisation von Beschaffungsstrategie abhängig

Wahl ob zentral oder dezentrales Lager abhängig von...
- Auftragsvolumen
- Auftragsstruktur
- Sortimentsbreite
- Artikelstruktur im Lagerystem

#### Eigenschaften verschiedeneer Lagersysteme
- Entscheidung ob statische oder dynamische Lagerung nach Art der Lagermittel
    - statisch: Lagergut ist von Einlagerung bist zur Auslagerung in Ruhe
    - dynamisch: Lagergut wird während des Lagerns bewegt

Auswahl des Lagertyps abhängig von Raum, Zugriffsart/-häufigkeit, Art und Maße der zu lagernden Objekte

#### Kosten von Lagersystemen
Lager können charaktisiert werden durch
    - Leistungsdaten (statisch, dynamisch)
    - Nutzungsgrade (Flächennutzungsgrad, Raumnutzungsgrad)
    - Kostenkennzahlen
automatisches Verschieberegal - teuerste Ausführungsform


### Transportsysteme

#### Ausführung und Aufgaben
Materialfluss: Hauptaufgabe ist das Transportieren
diese aufgabe umfasst Ein- und Auslagerung, sowie innerhalb des Produktionssystems

Nebenaufgaben: Verteilen, Sammeln, Puffern

Verteilen:
    - Vorgabe der Verteilung von Gütern an mehrere Abgabestationen
Sammeln:
    - Zusammenführung von Gütern von mehreren Quellen
Puffern:
    - Ausgleichen kleiner zeitlicher Unterschiede zwischen Transport- und Bearbeitungsvorgang

Unterscheidung in:
    - Stetigförderer
    - Unstetigförderer

#### Organisation des Transportsystems
    - Transportorganisation basiert auf der Anordnung der Bearbeitungsstationen
    - Organisationsformen: Direkt, Stern-Verkehr, Ringverkehr
        * Direktverkehr:
            wahlfreie Versorgung
            + Hohe Flexibilität
            - hoher Steuerungsaufwand
            - geringe Übersichtlichkeit
        * Stern-Verkehr
            paralell sequentielle Versorgung
            setzt Stetigkeit im Materialfluss voraus
                wird gewährleistet durch feste Fahrpläne
            - hoher Steuerungsaufwand
        * Ringverkehr
            sequentielle Versorgung
            + geringer Steuerungsbedarf durch festen Fahrplan

#### Steuerungsprinzipien von Transportystemen
    - materialflusssteuernde Systeme
    - materialflussgesteuerte Systeme

basieren auf Bring- und Holprinzip
    Bringprinzip:
        Materialflussvorgänge langfristig vorgeplant, abhängig vom Produktionsprogramm
        - Risiko von Bestandsanwachsen bei Laufzeitverschiebung oder Störungen
    Holprinzip:
        Materialbedarf von nachgelagerter Station bei vorgelagerter Station angefordert
        kurzfristige und schnelle Reaktion auf eingehende Anforderungen, Materialstauungen vermieden

Materialflussgesteuerte Systeme: Fertigungsablauf von Materialfluss gesteuert
- Häufig in materialflussorientierter Großserienfertigung verwendet (z.B. Automobilindustrie)

#### Eigenschaften verschiedener Transportsysteme
- Organisation des Transportsystems beeinflusst die Wahl der geeigneten Transportmittel
- Wahlfreie, direkte Organisation erfordert unabhängige, unverkettete Transportmittel
- enge Beziehung zwischen Anordnungsplanung, Transportorganisation und Wahl der Transportmittel
- Flexibilität und Automatisierbarkeit oft im Widerspruch

- Stetigförderer: Leicht automatisierbar, aber schwer an Layout- oder Mengenänderungen anzupassen
    - sind meist günstiger (da geringerer technischer Aufwand)
- Unstetigförderer: Flexibler, schwerer zu automatisieren, erfordert höheren technischen Aufwand

### Handhabungseinrichtungen
#### Aufgaben
Handhabungsvorgänge verbinden Transport-, Lager- und Bearbeitungsvorgägne

- automatisch oder manuell
-

#### Anforderungen und Ausführungsformen



## Planungsschritte
1. Erfassung vorhanderener Materialflusseinrichtungen
2. Ermittlung der Materialflussbeziehungen
3. Festlegung der Verkettungsstruktur, Betriebsmittelaufstellungsplanung
4. Erarbeitung von Materialflusssystemvarianten
5. Dimensionierung von Materialflusssystemvarianten

### Einflussgrößen
 Dimensionierung soll nicht von vornherein mit großem Spielraum erfolgen, sondern Systeme sollen flexibel und skalierbar sein
- Flexibilität: Varianten oder verschiedene Produkte können von den gleichen Systemen transportiert, gespeichert und gehandhabt werden, ohne aufwendige Anpassungen
- Skalierbarkeit: Ein Transportvorgang kann zunächst manuell durchgeführt werden, bei steigenden Stückzahlen später auch automatisch mit geeigneten Transporteinrichtungen.

## Funktionsplan
- Prozesskettendarstellung dient als Basis für Entwicklung von Lösungsalternativen für jeden Einzelprozess
- Untersuchung, ob Handhabungs- oder Transportvorgang manuell oder automatisch ablaufen soll
- Kombination verschiedener Teillösungsvarianten führt zu verschiedenen Gesamtlösungen

### Modellbildung bei der Ablaufsimulation
Einsatz nich t auf logische Systeme beschränkt - alles möglich solange Dauer statisch ist
Darstellung in Prozessketten

### Ablaufsimulation
Simulationsmodell wird erstellt -> Validierung
Kenngrößen für Validierung:
    - Bestand eines Puffers
    - Verweilzeit eines Teils im Puffer
    - Gesamte Durchlaufzeit eines Teils

Bewertung und Optimierung von Planungsalternativen beeinflussen vorgelagerte Planungsschritte

# 3.1 Bedeutung und Aufgabe der Materialdisposition
= Materialdisposition ist Teil der Produktionsplanung und -steuerung
parallel zur Entwicklung/Konstruktion und Arbeitsplanung im Auftragsdurchlauf durchgeführt
- Informationen werden an die Arbeitssteuerung weitergeleitet.
- Materialdisposition **plant Materialbedarf und -Beschaffung** basierend auf dem aktuellen Produktionsprogramm, einschließlich Lagerhaltung.

- Materialdisposition umfasst:
	- Bedarfsermittlung, 
	- Bestandsermittlung, 
	- Bestellplanung

## 3.2.1 Datenquellen der Materialdisposition
Produktionsprogramm gibt verbindlich vor: 
- welche Produkte, 
- in welchen Mengen und 
- zu welchen Zeitpunkten gefertigt werden sollen.

- Das Programm bildet die Basis für weitere Prozessplanung, definiert Primärbedarf für Materialdisposition und Eckdaten für Terminierung, Steuerung und Kontrolle des Produktionsprozesses.
- Materialdisposition erhält Informationen aus Konstruktion, Beschaffungswesen und Wareneingang.
- Konstruktion liefert wichtige Informationen in Form von Stücklisten zum Aufbau der Produkte.

### 3.2.2.1 Mengenstückliste
enthalten eine unstrukturierte Auflistung aller Einzelteile mit ihren Mengen in einem Erzeugnis.
- Sie zeigen nicht die Hierarchie oder Gliederungsebenen der Teile oder in welche Baugruppe sie eingehen.

Primäre Verwendung: 
Materialdisposition für die genaue Mengenermittlung der Einzelteile in einem Produkt.

### 3.2.2.2 Strukturstückliste
= zeigen die hierarchische Gliederung eines Produkts und dessen Zusammensetzung über alle Fertigungsstufen.
- Enthält alle Baugruppen, Teile und Rohmaterialien.

Nachteil: Wiederholte Auflistung ganzer Baugruppen mit ihren Elementen bei mehrfachem Auftreten im Produkt; kann bei vielen Teilen und mehrstufigem Produktionsprozess unübersichtlich werden.

- Änderungen in komplexen Produkten erfordern hohen Änderungsaufwand.
- Geeignet für Terminplanung und langfristige Materialbereitstellungsplanung.

### 3.2.2.3 Baukastenstückliste
= enthalten nur Baugruppen oder Einzelteile der nächsten Fertigungsstufe für jedes Endprodukt oder jede Baugruppe.
- Reduzieren den Gesamtumfang der Stücklisten, da Wiederholungsbaugruppen nur einmal aufgeführt werden.
- Vorteil: Gute Übersichtlichkeit, dient als Arbeitsunterlage für den Werkstattbereich.
- Geringerer Änderungsaufwand im Vergleich zu anderen Stücklistenarten.
- Gesamtzusammenhang einer Erzeugnisstruktur ist nur erkennbar, wenn zusammengehörige Baukastenstücklisten in Form einer Strukturstückliste ausgegeben werden.

### 3.2.2.4 Variantenstücklisten
- Varianten eines Erzeugnisses weisen oft nur geringfügige Unterschiede in einzelnen Teilen auf.
- Spezielle Stücklistenformen wurden entwickelt, die nur die veränderten Teile beschreiben.

Zwei Formen zur Darstellung von Varianten in Stücklisten:
  - Plus-Minus-Stückliste: Enthält alle Elemente aller Varianten; markiert zu ersetzende Teile mit einem Minus und hinzuzufügende Teile mit einem Plus in einem Variantenfeld.
  - Gleichteilstückliste: Trennt gleiche Teile von unterschiedlichen Bauteilen; für jede Variante existiert eine separate Stückliste, die auf die gleichen Teile in der ersten Zeile hinweist.

### 3.2.2.5 Teileverwendungsnachweise
    - Teileverwendungsnachweise zeigen für jedes Teil mit seiner Menge, in welchen übergeordneten Gruppen und Erzeugnissen es enthalten ist.
    - Konstruktion nutzt sie für schnellen Nachweis bei Änderungen, besonders bei funktionswichtigen Teilen in neuen Produktgenerationen.
    - Materialdisposition nutzt sie, um bei Ausschuss oder Lieferverzögerungen Prioritäten und Mengen für betroffene Aufträge festzulegen.

### 3.2.2 Stücklisten
- Stücklisten sind tabellarische Verzeichnisse, die zeigen, welche Rohmaterialien, Einzelteile und Baugruppen in ein Endprodukt eingehen und in welchen Mengen.

Ihre Aufgaben umfassen:
   - Darstellung der Erzeugnisstruktur von der Konstruktion bis zur Montage, dem Versand und dem Rechnungswesen.
   - Grundlage zur Mengenbestimmung und Festlegung von Terminen.
   - Liefern von Daten für Arbeitspläne und Bestellunterlagen.

Arten von Stücklisten nach Aufbau:
   - Mengenstückliste,
   - Strukturstückliste,
   - Baukastenstückliste,
   - Variantenstücklisten.
   
   Arten von Stücklisten nach Verwendungszweck:
   - Eigenfertigungsstücklisten,
   - Kaufteilstücklisten,
   - Montagestücklisten,
   - Materialstücklisten.

## 3.3 Vorgehensweise der Materialdisposition
drei Teilschritte:

1. Bedarfsermittlung:
   - Ermittlung des Bedarfs für die nächste Planungsperiode aus Quellen wie Produktionsprogramm und Stücklistenauflösung.

2. Bestandsermittlung:
   - Überprüfung des vorhandenen Materialbestands im Lager.

3. Bestellplanung:
   - Verwendung der Daten aus Bedarfsermittlung und Bestandsermittlung.
   - Auswahl einer geeigneten Beschaffungsart und rechtzeitige Auslösung der Beschaffung, um die Materialien fristgerecht in der benötigten Menge bereitzustellen.

### 3.3.1.1 Bedarfsarten
Drei verschiedene Bedarfsarten: Primär-, Sekundär- und Tertiärbedarf

Berücksichtigung von Lagerbeständen:
  - Bruttobedarf: Gesamt benötigte Materialmenge für Primär-, Sekundär- und Tertiärbedarf.
  - Nettobedarf: Differenz zwischen Bruttobedarf und verfügbarem Lagerbestand.

### 3.3.1.2 Methoden der Bedarfsermittlung
- Bedarfsplanung: Bestimmung der Mengen und Zeitpunkte des Bedarfs.

Methoden: 
- deterministisch (auf Basis von Aufträgen), 
- stochastisch (Vergleichswerte, Prognose), 
- heuristisch (Erfahrung, Analogschätzung)

- **Deterministische Methode:** nutzt vorhandene Aufträge für genaue Bedarfsermittlung.
- **Stochastische Methode:** prognostiziert zukünftigen Verbrauch ohne konkrete Aufträge, unter anderem mittels Methoden wie kleinsten Quadraten, gleitendem Mittelwert und exponentieller Glättung.
- **Heuristische Methode:** basiert auf Erfahrung und Schätzung oder Analogie zu vergleichbaren Materialien/Bauteilen.
- ABC-Analyse als Hilfsmittel zur Auswahl der Planungsmethode.

### 3.3.1.3 ABC-Analyse
Zusammenfassung:

- ABC-Analyse ordnet Lagerpositionen nach Wichtigkeit anhand des Mengen-/Wert-Verhältnisses.

Klassifizierung in 
- A (5-10% Positionen mit 80% Jahresverbrauch), 
- B (ca. 20% mit 15% Verbrauch)
- C (70% Artikel mit 5% Verbrauch)

- A-Positionen erhalten besondere Beachtung bei der Materialbedarfsplanung, 
- B-Positionen weniger, 
- C-Positionen haben geringen Einzelwert.

### 3.3.1.4 XYZ-Analyse
- Auswahl des Bedarfsermittlungsverfahrens basiert auf Artikelwert und zeitlichem Verbrauchsverlauf.

ABC-Analyse klassifiziert Teile in 
- X (konstanter Verbrauch, hohe Vorhersagegenauigkeit), 
- Y (schwankender Verbrauch, mittlere Genauigkeit), 
- Z (unregelmäßiger Verbrauch, geringe Genauigkeit)

## 3.3.2 Bestandsermittlung
= überprüft Vorhandensein und Menge von benötigten Rohmaterialien/Einzelteilen im Unternehmen.
- Ziel: Aktuellen Bestand von Materialien zu ermitteln, um Verfügbarkeit für die Produktion sicherzustellen.

### 3.3.2.1 Lagerbewegungen im Grundmodell
- Lagerung unterbricht Materialfluss, erhöht Durchlaufzeiten und bindet Kapital.
- Lagerung bedeutet in der Regel keine Wertsteigerung, daher unerwünscht aus Kostensicht.
- Lagerung als Puffer gegen mangelnde Termintreue der Lieferanten und Kapazitätsschwankungen in der Fertigung notwendig.

### 3.3.2.2 Lagerdaten
- Lagerbestandsbestimmung durch Erfassung von Lagerbewegungen.
- Lagerdaten in Stammdaten und Bewegungsdaten unterteilt.
- Stammdaten: Enthalten längerfristig festgelegte Kenngrößen.
- Bewegungsdaten: Erfassen jede Lagerbewegung, beschreiben aktuelle Lagerbestände.

## 3.3.3 Bestellplanung
Bestellplanungsaufgaben:
  - Festlegen der Beschaffungsart
  - Berechnung der Bestellmenge
  - Festlegung des Bestellzeitpunkts oder Bestellrhythmus

- Ziel: Bereitstellung benötigter Teile und Rohmaterialien zur richtigen Zeit.

### 3.3.3.1 Planung der Beschaffungsart
Bestellplanungsstrategien: 
  - Terminbezogene Beschaffungsauslösung
  - Bestandsbezogene Beschaffungsauslösung
  - Bedarfsbezogene Beschaffungsauslösung

- Entscheidung abhängig von der Art der Fertigung.
- Einzelfertigung: Bedarfsbezogene oder verbrauchsbezogene Beschaffung, abhängig von Teilekosten.
- Massenfertigung: Terminbezogene oder verbrauchsbezogene Beschaffung, abhängig von Bedarfsregelmäßigkeit.

#### 3.3.3.1.1 Materialbeschaffung in Abhängigkeit von der ABC- und XYZ-Analyse
Mögliche Beschaffungsprinzipien: 
  - Vorratshaltung (bestandsbezogene Auslösung)
  - Einzelbeschaffung im Bedarfsfall (bedarfsbezogene Auslösung)
  - Einsatzsynchrone Anlieferung (terminbezogene Auslösung)

- Vorratshaltung: Angemessener Vorrat, unempfindlich gegenüber Marktstörungen, aber erhöhte Durchlaufzeit und Lagerkosten.
- Einzelbeschaffung im Bedarfsfall: Geringe Kapitalbindung, aber hoher Planungs- und Beschaffungsaufwand.

- Einsatzsynchrone Anlieferung (Just-in-Time): Definierte Mengen zu festen Terminen entsprechend dem Produktionsprogramm, vorwiegend in Großserien- und Massenfertigung eingesetzt, minimiert Zwischenlagerung.

### 3.3.3.2 Optimale Bestellmenge, optimale Losgröße
- Berechnung der Bestellmengen und Auftragslosgröße notwendig unabhängig von Eigenfertigung oder Fremdbezug.
- Fremdbezogene Teile: "Optimale Bestellmenge"
- Eigengefertigte Teile: "Optimale Losgröße"
- Beide repräsentieren die Menge mit minimalen Kosten aus Beschaffung und Lagerung.

Berechnung kimmt hoffntlich nit lol

### 3.3.3.3 Planung des Bestellzeitpunktes
 Bestellzeitpunkt bei bestandsbezogener oder verbrauchsbezogener Materialbeschaffung ist bedeutend in der Materialdisposition.
- Praktisch angewandtes Verfahren: Bestellpunktverfahren.

Zwei Hauptziele:
    - Minimierung der Lagerbestände
    - Vermeidung von Materialengpässen oder Fehleinheiten

- Zielkonflikt: Späte Bestellauslösung vs. frühzeitige Bestellauslösung zur Risikominimierung.
- Bestellpunkt (Bestellauslösebestand) definiert: Berücksichtigt geschätzten Materialbedarf während der Wiederbeschaffungszeit.

Sicherheitsbestände gleichen Verbrauchsschwankungen und Beschaffungszeitänderungen aus, verursachen jedoch zusätzliche Lagerhaltungskosten.
- Geringe Reservebestände erhöhen das Risiko von Fehlmengen und Fehlmengenkosten.
- Lösung: Ermittlung eines optimalen Sicherheitsbestands, der das Kostenminimum aus Lagerhaltungskosten und Fehlmengenkosten darstellt.

### 3.3.3.4 Optimaler Bestellrhythmus
 Bestellrhythmusverfahren: Bestellungsauslösung bei terminbezogener Materialbeschaffung.
- Im Gegensatz zum Bestellpunktverfahren wird ein fester Zeitpunkt für Bestellung festgelegt.
- Bei Erreichen des festen Zeitpunkts wird der vorhandene Materialbestand anhand der Lagerdaten ermittelt, und die Bestellmenge wird daraus abgeleitet.
- Periodische Vorratsüberprüfung ist notwendig für reibungslosen Ablauf des Systems.
- Ziel: Minimierung der Kosten, abhängig von der Bestellperiodenlänge.

## 3.3.4 Rechnerhilfsmittel in der Materialdisposition
Ziel der PPS-Systeme: Aufwandsarme Speicherung und Bearbeitung durch Reduktion auf wesentliche Daten.
- Zentrale Datenbank ermöglicht ständige Aktualisierung und Bereinigung von Informationen.
- Simultane Planung von Menge und Zeit im PPS-System nicht durchführbar, daher Sukzessivplanungskonzept.
- „Rollende Planung“: Planungsfunktionen nacheinander mit zunehmender Detaillierung und abnehmendem Planungshorizont.

## 3.4.1 Zu den indirekten Bereichen Marketing, Vertrieb und Einkauf
 Materialdisposition initiiert Materialbeschaffung und überwacht Lagerhaltung.
- Diese Aufgaben führen zu Bestellungen: Fremdbezug an Materialeinkauf, eigene Fertigung an Fertigung.
- Aktuelles Produktionsprogramm bildet Grundlage für Dispositionsprozesse.

Produktionsprogramm an Vertrieb und Marketing weitergegeben:
  - Dient zur Auftragsbestätigung.
  - Nutzen für Planung von weiteren Aufträgen und Angebotserstellung.

## 3.4.2 Zur Arbeitssteuerung
- Materialdisposition im Auftragsdurchlauf zwischen Arbeitsplanung und Arbeitssteuerung.
- Ausgangsinformation: Fertigungsaufträge und Stücklisten an Arbeitssteuerung.
- Fertigungsaufträge informieren über zukünftige Belastung der Fertigung, werden für Kapazitäts- und Durchlaufzeitterminierung genutzt.
- Stücklisten, insbesondere Strukturstücklisten, sind entscheidende Grundlage für Fertigung und Montage, bieten Informationen über den Produkt­aufbau.

## 3.5 Zukünftige Entwicklungen
Dezentrale Lagerhaltung, Gruppenverantwortung, langfristige Lieferantenbeziehungen mit Fokus auf termingerechte Lieferung und Qualitätssicherung im JIT-Kontext.

# 4. Arbeitssteuerung
## 4.1.1 Die Arbeitssteuerung im Auftragsablauf und ihre Aufgaben
Hauptaufgaben:
    Festlegung von
        - Zeitpunkt
        - Ort
        - Häufigkeit
        - Verantwortlichkeiten
    für die Herstellung von Teilen oder Erzeugnissen in der Fertigung

Ziel: Fertigungsaufträge termingerecht und kostengünstig umzusetzen, indem Planung, Steuerung u
nd Kontrolle des Produktionsprozesses durchgeführt werden.

Informationen aus der Arbeitssteuerung werden an direkte und indirekte Bereiche weitergegeben, und zukünftige Entwicklungen der Arbeitssteuerung werden skizziert.
### 4.1.2 Ziele und Probleme der Arbeitssteuerung
Ziele umfassen: 
    - Maximierung der Termintreue, 
    - Minimierung der Durchlaufzeiten, 
    - Maximierung der Flexibilität, 
    - Minimierung von Beständen und Kapitalbindung, 
    - Maximierung der Kapazitätsauslastung und 
    - Sicherstellung des Qualitätsstandards

Diese Ziele lassen sich in interne (z. B. Bestände, Kapitalbindung, Auslastung) und externe (z. B. Termintreue, Durchlaufzeit, Flexibilität) Zielgrößen unterscheiden.
Die Optimierung der Zielgrößen ist problematisch, da sie sich gegenseitig beeinflussen.

### 4.1.3 Stufen der Arbeitssteuerung
    1. Planung des aktuellen Produktionsprogramms
    2.   Arbeitsplanung - Arbeitspläne
         Materialdisposition - Fertigugnsaufträge
    3. Arbeitssteuerung
        - Feinplanung des Produktionsprozesses -> Termininierte Fertigungsaufträge
        - Steuerung und Kontrolle des Produktionsprozesses -> Fertiggestellte Aufträge

## 4.2 Eingangsinformationen für die Arbeitssteuerung
- benötigt Infos von verschiedenen Unternehmensbereichen
- bieten detaillierte Arbeitsanweisungen und unterstützen die terminliche Planung der Aufträge in der Arbeitssteuerung.

## 4.3.1 Feinplanung des Produktionsprozesses
- Losgrößenplanung: Umfasst die Zusammenfassung einzelner Fertigungsaufträge zu Losen, wobei größere Lose niedrigere Auftragswechselkosten, aber höhere Lagerhaltungs- und Durchlaufzeiten bedeuten.
Durchlaufzeit: Gesamtzeit eines Auftrags (von Freigabe bis letzten Arbeitsschritt)
Auftrags- und anlagenbezogene Termin- und Kapazitätsplanung: Ziel: Arbeitsabläufe so zu planen dass Ziele der Arbeitssteuerung erreicht werden
Planungsverfahren: Verwendung von Prioritätsregeln

### 4.3.1.2 Durchlaufzeit in der Fertigung
Auftragsdurchlaufzeit: Gesamtzeit des Auftrags für alle Phasen der technischen Auftragsabwicklung
 = Summe der Durchlaufzeiten der einzelnen Arbeitsgänge, bestehend aus Übergangszeit und Durchführungszeit

Die Übergangszeit teilt sich in "Liegezeit nach Bearbeitung", Transportzeit und "Liegezeit vor Bearbeitung", während die Arbeitsgangdurchlaufzeit aus Rüstzeit und Bearbeitungszeit besteht.

Die Durchlaufzeit bildet die Grundlage für die Planung der Arbeitssteuerung und wird verwendet, um den Zeitpunkt für die Auftragsfreigabe in der Fertigung zu bestimmen, wenn die Terminplanung rückwärts erfolgt, basierend auf dem zugesagten Liefertermin und der Soll-Durchlaufzeit.

### 4.3.1.3 Auftrags- und anlagenbezogene Termin- und Kapazitätsplanung
1. Terminplanung
2. Kapazitätsplanung (auf Fertigung konzentriert)

Die Planung basiert auf der Optimierung der Abarbeitungsreihenfolge, um Ziele wie Durchlaufzeit, Kapazität, Bestand, Flexibilität und Termintreue zu erreichen.

### 4.3.1.4 Methoden der Planung des Produktionsprozesses
- Global vs. Lokal
    gesamten Planungsbereich vs lokale Verfahren
    global: berücksichtigt Auftragsreihenfolgeplanung aller Aufträge und Ressourcen eines Bereichs
    lokal: optimieren Planung auf Unterebene mit lokalen Optimierungskriterien
- Deterministisch vs. Heuristisch
    analytisch/statistisch genau vs. Erfahrungswerte

### 4.3.1.5 Klassische Auftragsplanung nach MRP
MRP I (Material Requirements Planning)
    ersetzt verbrauchsgesteuerte durch deterministische (analytische/statistische)
MRP II (Manufacturing Resource Planning)
    berücksichtigt nicht nur Materialbedarf, sondern auch Finanz-, Maschinen-, Werkzeug,- und Personalbedarf
Erfordert hohe Aktualität verschiedener Informationssysteme

### 4.3.1.6 MRP II Ablauf
welche Ressourcen zu welchen Zeitpunkt in welcher Menge?
kann Kapazitätsprobleme im Produktionsplan identifizieren

### 4.3.1.7 Optimized Production Technology - OPT
- Engpässe optimal zu belegen
- globales deterministisches Verfahren zur Maximierung des Ausstoßes
- Kategorisierung von Fertigungsaufträgen in
    - Aufträge mit Arbeitsvorgängen auf kritischen Betriebsmitteln
    - Aufträge mit Arbeitsvorgängen auf unkritischen Betriebsmitteln
9 Regeln:
1. Fertigungsdurchfluss, nicht die Kapazität abgleichen
2. Nutzungsgrad eines Nicht-Engpasses wird nicht durch dessen Kapazität bestimmt sondern durch die andere Begrenzung des Gesamtdurchlaufs
3. Bereitstellung und Nutzung einer Kapazität sind nicht gleichbedeutend
4. im Engpass verlorene Stunde ist für gesamtes System verloren
5. Stunde an einem Nicht-Engpass zu gewinnen ist bedeutungslos
6. Engpässe bestimmen Durchlauf und Bestände
7. Das Transportlos sollte meist nichts mit Fertigungslos zu tun haben
8. Bearbeitungslos muss variabel sein
9. Kapazitätsbelegung und Auftragsreihenfolge werden gleichzeitig betrachtet; Durchlaufzeiten sind Ergebnisse und können nicht im Voraus festgelegt werden

### 4.3.1.8 Prioritätsregeln
Prioritätsregeln sind Berechnungsschemata zur Bestimmung der Bearbeitungsreihenfolge von Arbeitsgängen in der Fertigung.

Lokales deterministisches Verfahren: Elementare und kombinierte Prioritätsregeln.

Elementare Prioritätsregeln: Eine Regel umfasst ein Kriterium zur Festlegung der Bearbeitungsreihenfolge.

Gängige Prioritätsregeln:
  - Kürzeste Operationszeitregel (KOZ)
  - Längste Operationszeitregel (LOZ)
  - Kleinste Restbearbeitungszeitregel (KRB)
  - Größte Restbearbeitungszeitregel (GRB)
  - First-In First-Out-Regel (FIFO oder FCFS)
  - Schlupfzeitregel

## 4.3.2 Steuerung und Überwachung des Produktionsprozesses Auftragsfreigabe und Arbeitsverteilung
Freigabe entspricht der Einleitung von Aufträgen in die Fertigung

Arbeitsverteilung:
    - Schnittstelle zwischen Planung und Umsetzung 
        Verwaltet:
            * freigegebene Aufträge
            * Materialbereitstellugn
            * Zuordnung von Arbeitsvorgängen zu Arbeitsplätzen
            * Ausgabe von Arbeitspapieren
            * Reaktionen auf Störung

zentraler vs. dezentraler Arbeitsverteilung
- Dezentrale Verteilung: z. B. der Meister übernimmt die Arbeitsteilung, kann dispositive Aufgaben erhalten und wird in modernen Arbeitsstrukturen wie Inselfertigung oder autonomen Gruppen eingesetzt.
  - Zentrale Verteilung: Arbeitsanweisungen gehen direkt an Produktionsarbeitsplätze.
  - Kombinierte Form: Zentrale Verteilung in Meisterbereiche mit anschließender dezentraler Verteilung.

### 4.3.2.1 Belastungsorientierte Auftragsfreigabe - BoA
= gegensatz zu MRP II
BaO basiert auf statistischer Betrachtung - nutzt Auftragsvorrat als Steuergröße

Grundgedanke: Zusammenhang von 
    - Durchlaufzeit
    - Auftragsbestand
    - Kapazität
    - Leistung
    für ein Arbeitssystem

Berücksichtigt Aufträge innerhalb festgelegter Zeitrahmen (Terminschranke), ordnet Aufträge nach Dringlichkeit und plant basierend auf verfügbaren Kapazitäten.

Vorraussetzungen:
    - Detaillierte Fertigungsanalyse
    - konstantes Produktspektrum
    - feste Liefertermine
    - bekannte Kapazitäten
    - gesicherte Materialverfügbarkeit
    - Nutzung einer Betriebsdatenerfassung

### 4.3.2.2 Kanban
Basierend auf verbrauchgesteuerten Supermarktprinzip udn verbundenen Regelkreisen

Dezentrale Steuerung und Synchronisation mittels Kanban-Karten

Bei Unterschreitung des Mindestbestands wird Kanban-Karte an die vorgelagerte Station gesendet, was einem Fertigungsauftrag entspricht
Kanban ermöglicht kurze Durchlaufzeiten, niedrige Bestände durch Verknüpfung von Aufträgen

Einsatzbereiche:
    - Begrenzte Variantenvielfalt und Komplexität
    - Fertigungsanlagen mit niedrigen Rüstzeiten und flussorientierter, harmonischer Produktion

### 4.3.2.3 Fortschrittszahlen
Informiert Produktionsbereiche und Zulieferer über Bedarfsverlauf aus Montageplanung

Darstellung der Teilbedarfe als Fortschrittzahlen ermöglicht Vergleich mit Soll-Produktionsmenge

Anwendungsbereiche mit Vorraussetzungen:
    - innerhalb des Unternehmens
        - Serienfertigung und Fließfertigung mit konstantem Materialbedarf
        - Nutzung einer Betriebsdatenerfassung
    - in einer Logistikkette
        - enge Geschäftsbeziehung (Hersteller/Zulieferer)
        - hohe Auftragswiederholhäufigkeit
        - Integration und Verknüpfung der Informationssysteme

### 4.3.2.4 Planungsmethoden und Freigabemechanismen
Planungsverfahren:
  - MRP I und II: Materialbedarfsplanung und Produktionssteuerung.
  - Optimized Production Technology - OPT: Konzentration auf Engpässe zur Maximierung der Produktivität.

Auftragsfreigabeverfahren:
  - Belastungsorientierte Auftragsfreigabe - BoA: Freigabe von Aufträgen basierend auf Kapazitätsauslastung.
  - Kanban: Pull-System zur Steuerung des Materialflusses.
  - Fortschrittszahlen: Überwachung und Steuerung des Auftragsfortschritts.

### 4.3.2.5 Auftragsfortschrittsüberwachung
kontinuierliche Überwachung des Auftragsfortschritts ist entscheidend für termingerechte und kosteneffiziente Produktion

manuelle vs automatisierte Arbeitssteuerung
Ziel beider: Anpassung von Belegungspläne an sich verändernde Fertigungssituationen, schnelle Reaktion auf Störungen und Einhaltung der Zielvorgaben

manuelle:
    - verursacht zeitliche Verzögerungen durch Menschlichen Delay

automatisierte:
    - integriert Infos aus Anlagen- und Auftragsüberwachung und Ergebnisse aus Störungsmanagement
    - kann dadurch schnell reagieren um geplante Ziele zu erreichen

## 4.3.3 Rechnerhilfsmittel in der Arbeitssteuerung
Produktionsplanung- und Steuerungssysteme
Feinplanung erfolgt über Leitsysteme
Um Planergebnisse in Fertigung umzussetzen und Anlagen zu steuern:
    Nutzung von 
    - CAM-Systeme
    - DNC- und MDE/BDE-Systemen
Aufwand für Durchlauf- und Kapazitätsterminierung sowie Maschinenbelegung steigt mit der Anzahl der Arbeitspläne exponentiell
- Lösungen erfordern verbesserte Rechnerhardware und Konzepte wie Intranet, dezentrale Datenhaltung und Komponentenware.

### 4.3.3.1 Plantafel und Gantt-Diagramm
Plantafel ist Instument für Termin- und Kapazitätsplanung sowie Auftragsüberwachung
visualisiert Maschinenbelegung, Arbeitsplatzreihenfolge, Belastungsprofile und Auftragsfortschritt

Darstellungsmethode = Gantt-Diagramm
- dieses zeigt Zeit auf vertikalen Zeitachse und Kapazitätseinheiten auf vertiakaler Achse. 
- Vorgänge werden als Balken dargestellt (mit Vorgangsdauer und Zeit für die jeweiligen Kapazitätseinheiten)


### 4.3.3.2 Betriebskalender

Ein Normalkalender numeriert Kalendertage ab einem bestimmten Stichtag. Zwei Varianten sind üblich:
  - Nur die tatsächlichen Arbeitstage werden nummeriert.
  - Alle Kalendertage werden nummeriert, wobei Arbeitstage und freie Tage gekennzeichnet werden.

Die zweite Variante ermöglicht eine einfachere Unterstützung durch EDV-Systeme im Unternehmen, z.B. bei der Definition von Überstunden an arbeitsfreien Tagen.

## 4.3.4 Qualitätsmanagementmethoden in der Arbeitssteuerung
TQM (Total Quality Management) - verfolgt ganzheitliche Qualitätsmanagementsstrategie im Unternehmen
- Externe Zielgrößen wie Termintreue und Durchlaufzeit stehen im Fokus der Verbesserung der Kunden-Lieferanten-Beziehungen.
- Verwendete Methoden sind spezifisch angepasste Qualitätsmanagementmethoden für die Arbeitssteuerung.

## 4.4 Ausgangsinformationen der Arbeitssteuerung
- Fertigungsbereich (Teilefertigung und Montage) hat engere Bindung und realisiert geplante Aufträge.

Von Arbeitssteuerung an Fertigung weitergegebene Fertigungsbelege (datentechnisch oder als Papierbelege):
  - Laufkarte
  - Terminkarte
  - Materialschein
  - Lohnschein
  - Kontrollschein
  - Plan- und Steuerbelege
Diese Belege unterstützen den Produktionsprozess.

Arbeitssteuerung versorgt weitere Unternehmensbereiche mit Informationen:
  - Vertrieb: Aktueller Produktionsplan und Auftragsfortschritt.
  - Marketing: Durchlaufzeiten und Termintreue der Fertigungsaufträge.
  - Management: Informationen über aktuelle Kosten bei Produktionsaufträgen.

## 4.5 Zukünftige Entwicklungen in der Arbeitssteuerung-Dezentralisierung
Arbeitssteuerung mit zwei künftigen Zielrichtungen:
  - Dezentralisierung von Planungsaufgaben: Verschiebung hin zur groben Vorausplanung, Mitarbeiter vor Ort übernehmen Planung, Steuerung und Kontrolle der Arbeitsschritte.
  - Erhöhter Einsatz von Informationssystemen: Verbesserte Mitarbeiterinformation über Fertigungsstand, schnellere Datenqualität und Neuplanungsgeschwindigkeit.

- Zukunftsaussicht: MRP II-Verfahren werden voraussichtlich mit realen Produktionsdaten in frühen Planungsstufen arbeiten können, obwohl sie heute noch nicht in der Lage dazu sind aufgrund fehlender ganzheitlicher Steuerungsfähigkeit.

### 4.5.1 Produktionsregelung
- Zielgrößenorientierte Produktion erfordert fortlaufende Überwachung des Auftragsfortschritts.
- Rückkopplungsprinzip der Regelungstechnik zur Kontrolle des Auftragsfortschritts.
- Anpassung der Reihenfolgeplanung basierend auf Anlagen- und Auftragsüberwachung sowie Störungsmanagement.

Ziele:
  - Flexible Anpassung der Belegungspläne an sich ändernde Fertigungssituationen.
  - Schnelle Reaktion auf Störungen oder Ausnahmesituationen.
  - Sicherstellung der Zielerreichung.

- Voraussetzungen: Einsatz von Informationssystemen von der Planung bis zum Arbeitsplatz, optimale Gestaltung des Reglersystems.

# 5. Maßnahmen in der Beschaffung
- Wertschöpfungsanteil durch Beschaffung höher als der durch Fertigung
- Qualitätssicherungsanforderungen in der Wareneingangsprüfung nehmen zu
 Abnehmer bestellen vermehrt Produktsysteme und vormontierte Baugruppen, was zu höherer Komplexität der Zulieferteile führt

## Ablauf der Beschaffung aus Sicht der Qualitätssicherung
- Produktbeschreibung
- Prüfplanung
- Lieferantenauswahl
- technische Lieferbedingung
- Erstmusterbewertung
- Bestellung
- Prüfauftrag
- Identprüfung
- Erstprüfung

## Lieferantenbewertung
zwei Teilbereiche:
- Auftrangsvergabe
- bei laufender Lieferung

Liefernatenauswahl beinhaltet Marktforschung und wertabalytische Betrachtung verschiedener Anbieter
Bewertet wird aus aleln Unternehmensbereichen, die im Kontakt zum Zulieferer stehen

Verfahren zur Bewertung:
    - Chechlistenverfahren
    - Punktbewertungsmethode
    - Geldwertmethode (Vergleich durch Zuteilung von Geldwerten zu Kriterien basierend auf Unternehmenszielen)

### Direkte Lieferantenbeurteilung
- Fokus auf forlaufender Überwachung der Produkt- und logistischen Qualität
- Produkt-Qualitätskennzahl basiert auf Wareineingangsprüfergebnissen, Nichterfüllung von Qualitätsmerkmalen als Fehler gewichtet
- Logistik-Qualitätskennzahl: Lieferzeit, Liefertreue und Reaktion auf Änderungswünsche

### Indirekte Lieferantenbeurteilung
- bewertet Arbeitsweise, Systematik, Einrichtungen und Mitarbeiter des Lieferanten

    - Qualitätsaudit:
        Überprüfung, ob Qualität den Anforderungen entspricht und wirksam ist
    - Systemaudit:
        QS-System prüft auf Effizienz und leitet Verbesserungen ein
    - Verfahrensaudit:
        Überprüft Verfahren und Arbeitsabläufe auf Einhaltung und Zweckmäßigkeit
    - Produktaudit:
        Feststellung von Fehlerschwerpunkten, systematischen Fehlern udn Entwicklungstrends von Fehlern am Produkt

## Wareneingangsüberprüfung
- Ziel ist Fehlervermeidung vor Serienanlauf
- Vollprüfung von Material, Geometrie, Funktion und Leistung
- Prüfdurchführung hängt von Erfahrungen vergangener Erstmusterprüfungen und laufender Lieferantenbeurteilung ab
- Ein hoch eingestufter Lieferant kann die Erstmusterprüfung selbständig durchführen

### Prüfstrategien im Wareneingang
- Prüfstrategie abhängig von Automatisierbarkeit der Messung, Gesamtstückzahlen, Vertrauensverhältnis
- Prüfdynamisierung in Abhängigkeit von positiven Prüfergebnissen bis zum Prüfverzicht
- Berücksichtigung der Auswirkung von Fehlern auf die weitere Fertigung oder das fertige Produkt

- Drei Fehlerarten nach DIN 55350:
    - Kritischer Fehler
    - Hauptfehler
    - Nebenfehler

5 Verschiedene Prüfungsarten:
    - Vollprüfung: Überprüfung des gesamten Loses auf Einhaltung aller Produktspezifikationen
    - 100%-Prüfung: Überprüfung ausgewählter Qualitätsmerkmale an allen Teilen eines Loses
    - Sortierprüfung: Aussortierung sämtlicher fehlerhafter Einheiten
    - Klassierprüfung: Einteilung der Prüflinge in Klassen für weitere Verwendung
    - Stichprobenprüfung an Teilmengen eines Loses mit festgelegtem Prüfmerkmal

### Schema einer fortlaufenden Wareneingangsprüfung
= kontinuierlicher Prozess

- Stichprobenprüfung der Ware nach festgelegtem Prüfverfahren
- Prüfergebnis wird in eine Datenbasis integriert

### Annahmestichprobenprüfung
- Einfach-Stichprobenprüfung als einfachste Form der Annahme-Stichprobenprüfung
    - Entscheidung über Annahme oder Rückweisung durch Prüfung einer einzigen Stichprobe

Zwei Möglichkeiten für Fehlentscheidungen: Fehler 1. Art (α-Fehler) und Fehler 2. Art (β-Fehler)
	- Fehler 1. Art ist das Abweisen eines Loses, obwohl der Fehleranteil den Grenzwert nicht überschreitet
	- Fehler 2. Art ist das Annehmen eines Loses, obwohl die Qualität zu gering ist

### Strategien der Beschaffung
- Verschiedene Stichprobensysteme für zählende und messende Abnahmeprüfung
- Prüfstrategie mit dem Ziel:
    - Wahrscheinlichkeit für Rückweisung guter Lose gering zu halten, wenn Qualitätslage vereinbart oder besser ist
    - Wahrscheinlichkeit für Annahme schlechter Lose gering zu halten, wenn Qualitätslage schlechter als vereinbart ist
    - Durchschlupf, also mittlere Qualitätslage angenommener Lose, prinzipiell zu begrenzen
    - Wirtschaftlichen Kompromiss zwischen Prüfaufwand und Risiko für Lieferant und Abnehmer zu finden
- Bedeutung der Eingangsprüfung durch Just-in-Time-Lieferung zurückgegangen
- Voraussetzung: Beschränkung auf wenige Zulieferer (Single Sourcing) und langfristige Bindung
