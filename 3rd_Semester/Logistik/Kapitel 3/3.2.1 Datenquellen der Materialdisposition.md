- Materialdisposition erfordert Planung des aktuellen Produktionsprogramms.
- Bei Massenfertigung basiert dies auf dem Absatzprogramm; bei Einzel- und teilweise Serienfertigung auf konkreten Aufträgen.
- Informationen stammen aus Vertrieb/Marketing und werden um Daten zu Produkten und Kapazität im Unternehmen ergänzt.
- Aktuelle Produktionsprogrammplanung bestimmt herzustellende Erzeugnisse nach Art, Menge und definiert Zeitraum.

Produktionsprogramm gibt verbindlich vor: 
- welche Produkte, 
- in welchen Mengen und 
- zu welchen Zeitpunkten gefertigt werden sollen.

- Das Programm bildet die Basis für weitere Prozessplanung, definiert Primärbedarf für Materialdisposition und Eckdaten für Terminierung, Steuerung und Kontrolle des Produktionsprozesses.
- Materialdisposition erhält Informationen aus Konstruktion, Beschaffungswesen und Wareneingang.
- Konstruktion liefert wichtige Informationen in Form von Stücklisten zum Aufbau der Produkte.