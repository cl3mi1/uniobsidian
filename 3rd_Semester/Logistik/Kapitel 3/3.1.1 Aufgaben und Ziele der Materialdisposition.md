Aufgaben der Materialdisposition:

1. Bestimmung des Bedarfs an Teilen und Materialien nach Art, Menge und Bereitstellungstermin.
2. Entscheidung über Eigenfertigung oder Fremdbezug von Teilen.
3. Materialbeschaffung nach Abgleich mit Lagerbeständen: Rohstoffe, Einzelteile, Baugruppen, Hilfs- und Betriebsstoffe.

Hauptziel: Termingerechte Bereitstellung der benötigten Positionen unter Berücksichtigung von Lieferschwankungen, Transportproblemen und anderen Logistik-Herausforderungen.

Weitere Ziele: Minimierung von Lagerbeständen und Erzielung günstiger Beschaffungspreise, obwohl diese Ziele konträr sind und eine genaue Kostenabwägung erfordern.

Optimierungsprozesse zielen darauf ab, Kosten zu minimieren.

Materialdisposition trifft entscheidende Entscheidungen wie Make-or-Buy und Auftragsfreigabe, was die Produktion maßgeblich beeinflusst, da sie das Auftragsvolumen und die Terminierung vorgibt.