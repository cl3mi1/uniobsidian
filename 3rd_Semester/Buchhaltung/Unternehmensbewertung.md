### Bewertungsanlässe
- rechliche Vorschriften
- vertragliche Vereinbarungen
- sonstige Gründe wie zB:
	- Börseneinführung
	- Erbteilung
	- Sanierung
	- Managervergütung
	- Abfindung
	- Umgründung(Verschmelzung, Einbringung, Zusammenschluss, Realteilung und Spaltung)
### KFS-BW 1
- Bewertungsobjekt -> zu bewertender/s Gegenstand/Unternehmen

- Schiedswert
	- der Wert den Verkäufer und Käufer akzeptieren -> muss nicht objektiv sein
- Entscheidungswert
- Argumentationswert
	- subjektiver Wert, den ich dahingehend anpasse, um meine Argumente darzustellen

- Discounted or Adjusted Value Verfahren (**gleiches Ergebnis**, beim Discounted alles einbezogen und beim Adjusted zweistufig gegliedert)
- Free Cashflow = Was man aus dem Unternehmen maximal entnehmen könnte (die Basisgröße der Entnahme)
