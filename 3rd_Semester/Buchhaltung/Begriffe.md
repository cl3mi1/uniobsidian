
##### Strategie
- Grundsatzbestimmung und strukturbestimmende Maßnahmen
- alles über 5 Jahre
- langfristige Fehler → können extrem komplex und schwer zu Lösen sein
#### Taktik
- zwischen 1 und 5 Jahre
- operativ bezieht sich immer nur auf 1 Jahr
- kurzfristige Fehler → können schnell und einfach behoben werden

#### Wann bin ich effizient, wann nicht? 
Wenn ein Prozess intern verbessert wird. Effizient = Kosteneinsparungen, schlanker strukturieren. 

**Effizienz**: Wirtschaftlich; Wenn Input und Output in einer besseren Relation stehen (d.h. ich brauche weniger Input, um das Gleiche zu erreichen.)

**Effektivität**: Nur auf Output gerichtet. 
