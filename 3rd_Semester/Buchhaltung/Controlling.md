- **Beeinflussung, Überwachung, Steuerung, Beherrschbarkeit**
- Wichtig im Prozessmanagement (Systembildend und Systemkoppelnd)
- unterstützt für entscheidungen
- Controlling als consulting in einer proaktiven Rolle (Rationalitätensicherung)


### Principal agent theory
Ein Principal überträgt eine Aufgabe an einen Agent → führt zu einem Resultat

1. Informationsasymmetrie → Misstrauens Spektrum (Hauptgrund für Kontrolle) 
2. Eigennutzung-Funktion beschränken durch Kontrolle

### Theory of Trust
Vertrauensbasis muss bestehen → heutige Arbeitswelt