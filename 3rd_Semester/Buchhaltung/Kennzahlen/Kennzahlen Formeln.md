| Kennzahl                  | Formel                                                                                   |    
| ------------------------- | ---------------------------------------------------------------------------------------- | 
| Gewinn                    | Umsatz - Kosten                                                                          |    
| EBITDA                    | Jahresüberschuss + Zinsen + Steuern + Abschreibung                                       |    
| EBIT                      | Jahresüberschuss + Zinsen + Steuern                                                      |    
| NOPAT                     | EBIT + Steuern                                                                           |    
| EVA                       | NOPAT - (NOA * WACC)                                                                     |    
| Reingewinn                | Erträge - Aufwendungen                                                                   |    
| Rohertrag u. -gewinn      | Umsatzerlöse - Wareneinsatz                                                              |    
| Gesamtkapitalrentabilität | = Umsatz                                                                                 |    
| Kapitalumschlag           | Umsatz / Gesamtkapital                                                                   |    
| Umsatzrentabilität        | Gewinn / Umsatz                                                                          |    
| ROI                       | Kapitalumschlag * Umsatzrentabilität                                                     |    
| WACC                      | ((Eigenkapital / Gesamtkapital) * iEK) + ((Fremdkapital / Gesamtkapital) * iFK * (1-s) ) |    
| ROCE                      | EBIT / eingesetztes Kapital                                                              |    
| Eigenkapitalquote         | (Eigenkapital / Gesamtkapital) * 100                                                     |    
| Fremdkapitalquote         | (Fremdkapital / Gesamtkapital) * 100                                                     |    
| Verschuldungsgrad         | (Fremdkapital / Eigenkapital) * 100                                                      |    
| Working Capital           | Umlaufvermögen - kurzfristige Verbindlichkeiten                                          |                                                                                              |     |
