
- Bildung von Kennzahlen
	- Auf Aussagekraft achten
	- Einschätzung über wichtige Zusammenhänge erlauben
	- müssen messbar sein
	- müssen spezifisch ausgestaltet sein
- vorliegende Daten entscheiden über Qualität der Kennzahl
- Falsche Information = Falsche Kennzahl = Falsche Entscheidung
- Ergänzung um qualitative Aussagen zur des Informationsgehalts


- EInzelkennzahlen
	- haben oft beschränkte Aussagefähigkeit
	- Implizieren oft umfangreichen Interpretationsspielraum
	- können widersprüchlich / verwirrend sein
		--> Koordinationsaufgabe beeinträchtigen
-  Lösungsversuch: Kennzahlensysteme
	- Reduzieren Interpretationsspielraum
	- Erhöhen Aussagekraft führungsrelevanter Aspekte