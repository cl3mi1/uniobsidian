
* **Analyse**
	- Beurteilung komplexer Sachverhalte
- **Steuerung**
	- Durchsetzung verbindlicher Ziele
	- Sachliche und personelle Koordination
<br/>
- **Steuerungskennzahlen**
	- Durchsetung verbindlicher Ziele
	- Willensdurchsetzung des Managements
	- Bildung durch Zerlegung der Unternehmensziele
- **Informationskennzahlen**
	- Alle Kennzahlen, die keine Steuerungskennzahlen sind
	- Unterstützung der Beurteilung komplexer Sachverhalte
	- Aufdecken von Entstehungsursachen
	- Bildung durch schrittweise Zerlegung des Sachverhalts