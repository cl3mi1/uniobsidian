### Funktionen von Kennzahlen und -Systemen
- Anregungsfunktion
	- Kontinuierliche Erfassung zur Identifikation von Auffälligkeiten und Veränderung
- Operationalisierungsfunktion
	- Bildung von Kennzahlen zur Operationalisierung von Zielen und der Zielerreichung
- Steuerungsfunktion
	- Nutzung von Kennzahlen zur Vereinfachung von Steuerungsprozessen
- Kontrollfunktion
	- Fortlaufende Erfassung von Kennzahlen zur Erkennung von Soll-Ist-Abweichungen und anschließenden Abeweichungsanalysen
