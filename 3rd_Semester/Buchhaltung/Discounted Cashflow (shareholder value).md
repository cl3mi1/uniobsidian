### Berechnen: Discounted Cashflow (shareholder value)
- die freien Cashflows (CFC) der betrachteten Jahre (t) werden mit (1+WACC) abgezinsst und anschließend summiert
- Die Summe wird mit der, nach dem gleichen Verfahren abgezinsten, Summe des Residualwertes (geschätzte freie Cashflows für die Zeit nach den betrachteten Jahren) und dem Wert des nicht-betriebsnotwendigen Vermögens (Wertpapieranlagen, Spekulationsgüter usw.) summiert.
- Von dieser Summe Fremdkapital des Unternehmens abziehen === **Shareholder Value**

#### Shareholder value (Aktionärswert)
- Marktwert des Eigenkapitals
- Shareholder-Value-Ansatz (Rappaport)
- Unternehmensgeschehen == Reihung von Kapitalflüssen
- = diskontierter, freier Cashflow abzüglich des Marktwertes des Fremdkapitals
- Ziel ist es, das Eigenkapital zu Marktwerten zu bewerten. 
	- zB: Was ist eine Aktie für den Aktionär wert?