###### Klausurrelevant
- Spread-Kennzahl, die länderspezifische Faktoren raus nimmt:
	- Steuern
	- Abschreibungen
- Hilft bei Vergleich von zwei Unternehmen in unterschiedlichen Ländern

- Residualgewinne
	- Einperiodisch
	- Vergangenheitsorientiert
	- Absolutgröße
	- Vergleichbar mit EVA

- Mögliche Berechnungen:
	- Capital Charge
	- **Value Spread-Formel**

CFROI: Verzinsung des CashFlows auf das eingesetzte Investment
CFROI - WACC = CF SPREAD
CE * CF SPREAD = CVA

Vergleiche von CVA und EVA zeigen mir, welches Land für mein Unternehmen, auf die Steuer Bezogen, interessanter ist.




**![cva](https://lh7-us.googleusercontent.com/2Su4SJ1lE85YyefDy6bAW6j7NjZ5QHXjgnb8hknpbe7JJ-NoHYM8BODyHuz7e3lHdPJfkKQScBQxc7a1Vr4bWZdhYfCNgV3gciHWIgIyDFJ1mt5sW5YTht7Bt2d-A529BQkh2zQ0SPUxu32ZeqKS2io "CVA")**