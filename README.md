# UniObsidian

uniObsidian is a public GitLab repository that I use to organize and share my university notes. The notes are primarily stored in Markdown files, making them easily readable and portable. To enhance the note-taking experience, I also utilize Obsidian's powerful canvas feature, which enables visual note-linking and brainstorming. While the Markdown files can be used in any editor, the full potential of this project is best realized when viewed and edited using Obsidian, providing an intuitive, interactive environment for learning and organizing information.

* if you want to add your notes to this project feel free to contact me!

## Getting Started

To use UniObsidian, follow these simple steps:

1. Clone the project to your local machine (or just download it without versioncontrol as a zip file):

    ```bash
    git clone https://gitlab.com/cl3mi1/uniobsidian.git
    ```

2. Open the project folder in Obsidian.

3. Start exploring and organizing your notes in a visually appealing canvas. Feel free to add your notes.

## Features

- **Markdown Files:** Leverage the power of Markdown for seamless note-taking.
- **Beautiful Canvas:** Enjoy an enhanced visual experience for your notes.
- **Version Control:** Utilize GitLab for version tracking and collaboration.

## Prerequisites

Make sure you have the following tools installed:

- [Git](https://git-scm.com/)
- [Obsidian](https://obsidian.md/)

## Contributing

If you want to contribute to this project, feel free to fork it and submit a pull request. I welcome any enhancements.

## Acknowledgments

- Obsidian Community
- GitLab

Happy note-taking!

![Alt text](image.png)
